﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1
{
    class Program
    {
        static void Main(string[] args)
        {
            Connect4 game = new Connect4();
            game.start();
            Console.WriteLine("Press enter to exit");
            Console.ReadLine();
        }
    }
}
