﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//add input validation, draw/win condition, handlingcolumnoverflow,
namespace Assignment1
{
    class Connect4
    {
        private String[,] board = createPattern();
        private bool winner = false;
        private bool end = false;
        private int roundNumber = 1;

        public Connect4()
        {

        }

        public static String[,] createPattern()
        {
            
            var board = new String[7, 15];

            
            for (int i = 0; i < board.GetLength(0); i++)
            {

                
                for (int j = 0; j < board.GetLength(1); j++)
                {
                    
                    if (j % 2 == 0) board[i, j] = "|";
                    else board[i,j] = " ";

                    if (i == 6) board[i,j] = "-";
                }

            }
            return board;
        }

        public void printPattern()
        {
            for (int i = 0; i < this.board.GetLength(0); i++)
            {
                for (int j = 0; j < this.board.GetLength(1); j++)
                {
                    Console.Write(this.board[i,j]);
                }
                Console.WriteLine();
            }
        }

        public void Turn()
        {
            bool correctInput = false;
            bool inputError = false;
            bool fullError = false;
            int input;

            while (correctInput == false)
            {
                printPattern();
                Console.Write("Drop a red disk at column (0–6): ");
                try
                {
                    input = Int32.Parse(Console.ReadLine());

                    if (input >= 0 && input <= 6)
                    {
                        fullError = false;
                        inputError = false;

                        for (int i = 5; i >= 0; i--)
                        {
                            int c = 2 * input + 1;

                            if (roundNumber % 2 == 0)
                            {
                                if (this.board[i, c] == " ")
                                {
                                    this.board[i, c] = "Y";
                                    fullError = false;
                                    correctInput = true;
                                    roundNumber++;
                                    break;
                                }
                            }
                            else
                            {
                                if (this.board[i, c] == " ")
                                {
                                    this.board[i, c] = "R";
                                    fullError = false;
                                    correctInput = true;
                                    roundNumber++;
                                    break;
                                }
                            }
                            fullError = true;
                        }
                    }
                    else
                    {
                        inputError = true;
                    }
                }
                catch
                {
                    inputError = true;
                }
                

               
                
                



                if (inputError == true)
                {
                    Console.WriteLine("Invalid input. Input a number between 0 and 6.");
                }


                if (fullError == true)
                {
                    Console.WriteLine("Invalid input. The column is full.");
                }
            }
        }


        public void checkWinner()
        {

            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 7; j += 2)
                {
                    if ((this.board[i,j + 1] != " ")
                    && (this.board[i,j + 3] != " ")
                    && (this.board[i,j + 5] != " ")
                    && (this.board[i,j + 7] != " ")
                    && ((this.board[i,j + 1] == this.board[i,j + 3])
                    && (this.board[i,j + 3] == this.board[i,j + 5])
                    && (this.board[i,j + 5] == this.board[i,j + 7])))
                    {
                        if (this.board[i, j + 1] == "Y")
                        {
                            printPattern();
                            Console.WriteLine("The yellow player won");
                            winner = true;
                        }

                        if (this.board[i, j + 1] == "R")
                        {
                            printPattern();
                            Console.WriteLine("The red player won");
                            winner = true;
                        }
                    }
                }
            }

            for (int i = 1; i < 15; i += 2)
            {
                for (int j = 0; j < 3; j++)
                {
                    if ((this.board[j,i] != " ")
                    && (this.board[j + 1,i] != " ")
                    && (this.board[j + 2,i] != " ")
                    && (this.board[j + 3,i] != " ")
                    && ((this.board[j,i] == this.board[j + 1,i])
                    && (this.board[j + 1,i] == this.board[j + 2,i])
                    && (this.board[j + 2,i] == this.board[j + 3,i])))
                    {
                        if (this.board[j, i] == "Y")
                        {
                            printPattern();
                            Console.WriteLine("The yellow player won");
                            winner = true;
                        }

                        if (this.board[j, i] == "R")
                        {
                            printPattern();
                            Console.WriteLine("The red player won");
                            winner = true;
                        }
                    }

                }
            }

            for (int i = 0; i < 3; i++)
            {
                for (int j = 1; j < 9; j += 2)
                {
                    if ((this.board[i,j] != " ")
                    && (this.board[i + 1,j + 2] != " ")
                    && (this.board[i + 2,j + 4] != " ")
                    && (this.board[i + 3,j + 6] != " ")
                    && ((this.board[i,j] == this.board[i + 1,j + 2])
                    && (this.board[i + 1,j + 2] == this.board[i + 2,j + 4])
                    && (this.board[i + 2,j + 4] == this.board[i + 3,j + 6])))
                    {
                        if (this.board[i, j] == "Y")
                        {
                            printPattern();
                            Console.WriteLine("The yellow player won");
                            winner = true;
                        }

                        if (this.board[i, j] == "R")
                        {
                            printPattern();
                            Console.WriteLine("The red player won");
                            winner = true;
                        }
                    }
                }
            }

            for (int i = 0; i < 3; i++)
            {
                for (int j = 7; j < 15; j += 2)
                {
                    if ((this.board[i,j] != " ")
                    && (this.board[i + 1,j - 2] != " ")
                    && (this.board[i + 2,j - 4] != " ")
                    && (this.board[i + 3,j - 6] != " ")
                    && ((this.board[i,j] == this.board[i + 1,j - 2])
                    && (this.board[i + 1,j - 2] == this.board[i + 2,j - 4])
                    && (this.board[i + 2,j - 4] == this.board[i + 3,j - 6])))
                    {
                        if (this.board[i, j] == "Y")
                        {
                            printPattern();
                            Console.WriteLine("The yellow player won");
                            winner = true;
                        }

                        if (this.board[i, j] == "R")
                        {
                            printPattern();
                            Console.WriteLine("The red player won");
                            winner = true;
                        }
                    }
                }
            }
        }

        public void start()
        {
            while ( end == false)
            {
                Turn();
                checkWinner();
                if(roundNumber > 42)
                {
                    Console.WriteLine("Nobody Wins");
                    winner = true;
                }

                if (winner == true)
                {
                    end = true;
                }
            }
            

        }
    }
}
